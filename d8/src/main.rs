use std::{env,fs,io::prelude::*};

fn countnum(n: u8, s: &[u8]) -> usize {
    s.into_iter().fold(0, |sum,u| {if *u == n {sum+1} else {sum}})
}

fn main() {
    let argv = env::args().collect::<Vec<String>>();
    let f = fs::File::open(argv[1].clone()).unwrap();
    let imgbytes = f.bytes().map(|b| {b.unwrap() - '0' as u8})
        .collect::<Vec<u8>>();
    let img = imgbytes.chunks(25*6).collect::<Vec<&[u8]>>();
    let minlayer = img.iter().min_by(|s1,s2| {
        let sum1 = countnum(0, s1);
        let sum2 = countnum(0, s2);
        sum1.cmp(&sum2)
    }).unwrap();
    println!("{}", countnum(1, minlayer) * countnum(2,minlayer));
    let mut flatimg = vec![];
    for i in 0..(25*6) {
        let latp = img.iter().map(|lay| {lay[i]});
        let mut color = 2;
        for c in latp {
            if c != 2 {
                color = c;
                break;
            }
        }
        flatimg.push(color);
    }
    for i in 0..6 {
        for j in 0..25 {
            print!("{} ", match flatimg[(25*i)+j] {
                0 => "\x1b[40m",
                1 => "\x1b[47m",
                _ => "\x1b[0m"
            });
        }
        print!("\x1b[0m\n");
    }
}
