use std::{fs,io,io::prelude::*};
use std::{iter, str, env};
use std::{thread,sync::mpsc};
use permutohedron::heap_recursive;

struct Machine <'a> {
    mem: Vec<i32>,
    pc: usize,
    input: Box<dyn Fn()->i32 + 'a>,
    output: Box<dyn Fn(i32)->() + 'a>
}

trait OpCode {
    fn exec(&self, machine: &mut Machine, mode: Vec<ParamMode>);
    fn do_halt(&self) -> bool {false}
}

#[derive(Debug)]
enum ParamMode {
    Indirect,
    Immediate
}

fn mode_from_bits(mbits: i32) -> Vec<ParamMode> {
    let mut curbits = mbits;
    let mut modes = vec![];
    while curbits != 0 {
        modes.push(match curbits % 10 {
            0 => ParamMode::Indirect,
            1 => ParamMode::Immediate,
            _ => panic!("unimplemented param mode {}", curbits % 10)
        });
        curbits /= 10;
    }
    modes
}

macro_rules! optab {
    { $($num:literal => $struct:ident
        $( [$machine:ident] ($($oarg:ident),*) $act:expr)?),* }
    => {
        $(
            struct $struct;
            $(
                impl OpCode for $struct {
                    fn exec(&self, $machine:&mut Machine, mode: Vec<ParamMode>){
                        let mut argc = 0;
                        $(argc += 1;
                          let $oarg = match mode.get(argc-1) {
                              None | Some(ParamMode::Indirect) =>
                                  $machine.mem[$machine.pc + argc],
                              Some(ParamMode::Immediate) =>
                                  ($machine.pc + argc) as i32
                          };)*
                        $act;
                        $machine.pc += argc + 1;
                    }
                }
            )?
        )*        
        fn opcode_from_i32(o:i32) -> (Box<dyn OpCode>, Vec<ParamMode>) {
            let code = o % 100;
            let modebits = o / 100;
            let mode = mode_from_bits(modebits);
            let op: Box<dyn OpCode>  = match code {
                $($num => Box::new($struct{}),)*
                _ => panic!("invalid opcode {}", o)
            };
            return (op, mode);
        }
    }
}

optab!{
    99 => OpTerm,
    1 => OpAdd [vm] (a, b, c) {
        vm.mem[c as usize] = vm.mem[a as usize] + vm.mem[b as usize];
    },
    2 => OpMul [vm] (a, b, c) {
        vm.mem[c as usize] = vm.mem[a as usize] * vm.mem[b as usize];
    },
    3 => OpIn [vm] (a) {
        vm.mem[a as usize] = (vm.input)();
    },
    4 => OpOut [vm] (a) {
        (vm.output)(vm.mem[a as usize]);
    },
    5 => OpJpT [vm] (cond, dest) {
        if vm.mem[cond as usize] != 0 {
            vm.pc = (vm.mem[dest as usize] - 3) as usize
        }
    },
    6 => OpJpF [vm] (cond, dest) {
        if vm.mem[cond as usize] == 0 {
            vm.pc = (vm.mem[dest as usize] - 3) as usize
        }
    },
    7 => OpLT [vm] (a, b, c) {
        vm.mem[c as usize] =
            if vm.mem[a as usize] < vm.mem[b as usize] {1} else {0};
    },
    8 => OpEQ [vm] (a, b, c) {
        vm.mem[c as usize] =
            if vm.mem[a as usize] == vm.mem[b as usize] {1} else {0};
    }
}

impl OpCode for OpTerm {
    fn exec(&self, _vm: &mut Machine, _mode: Vec<ParamMode>){}
    fn do_halt(&self) -> bool {true}
}

fn vm_exec(vm: &mut Machine) -> () {
    vm.pc = 0;
    loop {
        let (op, mode) = opcode_from_i32(vm.mem[vm.pc]);
        op.exec(vm, mode);
        if op.do_halt() {break;}
    }
}

fn vm_thread(prog: &Vec<i32>, inchan: mpsc::Receiver<i32>,
             ochan: mpsc::Sender<i32>)
             -> thread::JoinHandle<()> {
    let mem = prog.clone();
    thread::spawn(move || {
        let mut mac = Machine {
            mem: mem,
            pc: 0,
            input: Box::new(move || {inchan.recv().unwrap()}),
            output: Box::new(move |i| {ochan.send(i).unwrap_or(());})
        };
        vm_exec(&mut mac);
    })
}

fn vm_build_chain(prog: &Vec<i32>, vminits: Vec<i32>)
                  -> (Vec<thread::JoinHandle<()>>,
                      mpsc::Sender<i32>, mpsc::Receiver<i32>) {
    let (itx,irx) = mpsc::channel();
    itx.send(vminits[0]).unwrap();
    let mut threads = vec![];
    let orx = vminits.into_iter().skip(1).map(|i| {Some(i)})
        .chain(iter::once(None))
        .fold(irx, |irx, ino| {
            let (otx,orx) = mpsc::channel();
            match ino {
                Some(i) => otx.send(i).unwrap(),
                None => ()
            };
            threads.push(vm_thread(prog, irx, otx));
            orx
        });
    (threads, itx, orx)
}

fn vm_run_chain(prog: &Vec<i32>, vminits: Vec<i32>) -> i32 {
    let (threads, itx, orx) = vm_build_chain(prog, vminits);
    itx.send(0).unwrap();
    for thread in threads {
        thread.join().unwrap();
    }
    orx.recv().unwrap()
}

fn part_a(prog: &Vec<i32>) -> i32 {
    let mut results_a = vec![];
    heap_recursive(&mut vec![0,1,2,3,4], |perm| {
        results_a.push(vm_run_chain(&prog, perm.to_vec()));
    });
    results_a.into_iter().max().unwrap()
}

fn part_b(prog: &Vec<i32>) -> i32 {
    let mut results_b = vec![];
    heap_recursive(&mut vec![5,6,7,8,9], |perm| {
        let mut lanum = 0;
        let (threads,itx,orx) = vm_build_chain(prog, perm.to_vec());
        itx.send(0).unwrap();
        orx.iter().for_each(|i| {
            lanum = i;
            itx.send(i).unwrap_or(());
        });
        for thread in threads {
            thread.join().unwrap();
        }
        results_b.push(lanum);
    });
    results_b.into_iter().max().unwrap()
}

fn main() {
    let args = env::args().collect::<Vec<String>>();
    let f = io::BufReader::new(fs::File::open(args[1].clone()).unwrap());
    let prog = f.split(',' as u8)
        .filter_map(
            |o| str::from_utf8(&o.ok()?).ok()?.trim().parse::<i32>().ok()
        ).collect::<Vec<i32>>();
    println!("{}", part_a(&prog));
    println!("{}", part_b(&prog));
}
