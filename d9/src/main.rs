use std::{fs,io,io::prelude::*};
use std::{str, env, fmt};
use std::{thread,sync::mpsc};
 
struct Machine <'a> {
    mem: Vec<i64>,
    pc: i64,
    rbase: i64,
    input: Box<dyn Fn()->i64 + 'a>,
    output: Box<dyn Fn(i64)->() + 'a>
}
 
trait OpCode : fmt::Debug {
    fn exec(&self, machine: &mut Machine, mode: Vec<ParamMode>);
    fn do_halt(&self) -> bool {false}
}
 
macro_rules! intcode {
    {
        modes [$modemac: ident] ($argc:ident) {
            $($modenum:literal => $mode:ident $modeaction:expr),*
        }
        opcodes {
            $($num:literal => $struct:ident
              $( [$machine:ident] ($($oarg:ident),*) $act:expr)?),*
        }
    } => {
        #[derive(Debug)]
        enum ParamMode {
            Indirect,
            $($mode,)*
        }
        
        fn mode_from_bits(mbits: i64) -> Vec<ParamMode> {
            let mut curbits = mbits;
            let mut modes = vec![];
            while curbits != 0 {
                modes.push(match curbits % 10 {
                    0 => ParamMode::Indirect,
                    $($modenum => ParamMode::$mode,)*
                    _ => panic!("unimplemented param mode {}", curbits % 10)
                });
                curbits /= 10;
            }
            modes
        }

        fn modemaparg($modemac: &mut Machine, mode: &Vec<ParamMode>,
                      $argc:usize) -> i64 {
            match mode.get($argc-1) {
                None | Some(ParamMode::Indirect) =>
                    $modemac.mem[$modemac.pc as usize + $argc],
                $(Some(ParamMode::$mode) => $modeaction,)*
            }
        }
        
        $(
            #[derive(Debug)] 
            struct $struct;
            $(
                impl OpCode for $struct {
                    fn exec(&self, $machine:&mut Machine, mode: Vec<ParamMode>){
                        let mut argc = 0;
                        $(argc += 1;
                          let $oarg = modemaparg($machine, &mode, argc);
                          if $machine.mem.len() <= $oarg as usize {
                              $machine.mem.resize($oarg as usize + 1, 0)
                          })*
                        $act;
                        $machine.pc += argc as i64 + 1;
                    }
                }
            )?
        )*        
        fn opcode_from_i64(o:i64) -> (Box<dyn OpCode>, Vec<ParamMode>) {
            let code = o % 100;
            let modebits = o / 100;
            let mode = mode_from_bits(modebits);
            let op: Box<dyn OpCode>  = match code {
                $($num => Box::new($struct{}),)*
                    _ => panic!("invalid opcode {}", o)
            };
            return (op, mode);
        }
    }
}
 
intcode!{
    modes [vm] (argc) {
        1 => Immediate {vm.pc + argc as i64},
        2 => RBase {vm.mem[vm.pc as usize + argc] + vm.rbase}
    }
    opcodes {
        99 => OpTerm,
        1 => OpAdd [vm] (a, b, c) {
            vm.mem[c as usize] = vm.mem[a as usize] + vm.mem[b as usize];
        },
        2 => OpMul [vm] (a, b, c) {
            vm.mem[c as usize] = vm.mem[a as usize] * vm.mem[b as usize];
        },
        3 => OpIn [vm] (a) {
            vm.mem[a as usize] = (vm.input)();
        },
        4 => OpOut [vm] (a) {
            (vm.output)(vm.mem[a as usize]);
        },
        5 => OpJpT [vm] (cond, dest) {
            if vm.mem[cond as usize] != 0 {
                vm.pc = vm.mem[dest as usize] - 3
            }
        },
        6 => OpJpF [vm] (cond, dest) {
            if vm.mem[cond as usize] == 0 {
                vm.pc = vm.mem[dest as usize] - 3
            }
        },
        7 => OpLT [vm] (a, b, c) {
            vm.mem[c as usize] =
                if vm.mem[a as usize] < vm.mem[b as usize] {1} else {0};
        },
        8 => OpEQ [vm] (a, b, c) {
            vm.mem[c as usize] =
                if vm.mem[a as usize] == vm.mem[b as usize] {1} else {0};
        },
        9 => OpAdjRbase [vm] (rba) {
            vm.rbase += vm.mem[rba as usize];
        }
    }
}
 
impl OpCode for OpTerm {
    fn exec(&self, _vm: &mut Machine, _mode: Vec<ParamMode>){}
    fn do_halt(&self) -> bool {true}
}
 
fn vm_exec(vm: &mut Machine) -> () {
    vm.pc = 0;
    loop {
        let (op, mode) = opcode_from_i64(vm.mem[vm.pc as usize]);
        op.exec(vm, mode);
        if op.do_halt() {break;}
    }
}

fn vm_thread(prog: &Vec<i64>, inchan: mpsc::Receiver<i64>,
             ochan: mpsc::Sender<i64>)
             -> thread::JoinHandle<()> {
    let mem = prog.clone();
    thread::spawn(move || {
        let mut mac = Machine {
            mem: mem,
            rbase: 0,
            pc: 0,
            input: Box::new(move || {inchan.recv().unwrap()}),
            output: Box::new(move |i| {ochan.send(i).unwrap_or(());})
        };
        vm_exec(&mut mac);
    })
}

fn main() {
    let args = env::args().collect::<Vec<String>>();
    let f = io::BufReader::new(fs::File::open(args[1].clone()).unwrap());
    let prog = f.split(',' as u8)
        .filter_map(
            |o| {str::from_utf8(&o.ok()?).ok()?.trim().parse::<i64>().ok()}
        ).collect::<Vec<i64>>();
    let mut mac = Machine {
        mem: prog,
        rbase: 0,
        pc: 0,
        input: Box::new(move || {
            let mut ln = String::new();
            io::stdin().lock().read_line(&mut ln).unwrap();
            ln.trim().parse::<i64>().unwrap()
        }),
        output: Box::new(move |i| {
            println!("{}", i);
        })
    };
    vm_exec(&mut mac);
}
