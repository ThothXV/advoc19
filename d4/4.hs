irange = [245318..765747]

digits 0 = []
digits n = (digits $ n `div` 10) ++ [n `mod` 10]

hasdouble (x:y:xs) = x == y || hasdouble (y:xs)
hasdouble (x:[]) = False
hasdouble [] = False

inseq e (x:xs)
  | e == x = 1 + inseq e xs
  | otherwise = 0
inseq e [] = 0

exactdouble (x:xs)
  | inseq x (x:xs) == 2 = True
  | otherwise = exactdouble $ drop (inseq x xs) xs
exactdouble [] = False

isNonDec (x:y:xs) = x <= y && isNonDec (y:xs)
isNonDec (x:[]) = True
isNonDec [] = True

numSolutions pred =
  length . filter id . map pred . map digits

main = do
  putStrLn $ show $ numSolutions (\x -> isNonDec x && hasdouble x) irange
  putStrLn $ show $ numSolutions (\x -> isNonDec x && exactdouble x) irange
