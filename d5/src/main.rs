use std::fs; 
use std::io::prelude::*;
use std::io;
use std::str;

trait OpCode {
    fn exec(&self, mem:&mut Vec<i32>, pc: &mut usize, mode: Vec<ParamMode>);
    fn do_halt(&self) -> bool {false}
}

#[derive(Debug)]
enum ParamMode {
    Indirect,
    Immediate
}

fn mode_from_bits(mbits: i32) -> Vec<ParamMode> {
    let mut curbits = mbits;
    let mut modes = vec![];
    while curbits != 0 {
        modes.push(match curbits % 10 {
            0 => ParamMode::Indirect,
            1 => ParamMode::Immediate,
            _ => panic!("unimplemented param mode {}", curbits % 10)
        });
        curbits /= 10;
    }
    modes
}

macro_rules! optab {
    { $($num:literal => $struct:ident
        $( [$mem:ident] ($pc: ident) ($($oarg:ident),*) $act:expr)?),* }
    => {
        $(
            struct $struct;
            $(
                impl OpCode for $struct {
                    fn exec(&self, $mem:&mut Vec<i32>, $pc: &mut usize,
                            mode: Vec<ParamMode>){
                        let mut argc = 0;
                        $(argc += 1;
                          let $oarg = match mode.get(argc-1) {
                              None | Some(ParamMode::Indirect) =>
                                  $mem[*$pc+argc],
                              Some(ParamMode::Immediate) =>
                                  (*$pc + argc) as i32
                          };)*
                        $act;
                        *$pc += argc + 1;
                    }
                }
            )?
        )*        
        fn opcode_from_i32(o:i32) -> (Box<dyn OpCode>, Vec<ParamMode>) {
            let code = o % 100;
            let modebits = o / 100;
            let mode = mode_from_bits(modebits);
            let op: Box<dyn OpCode>  = match code {
                $($num => Box::new($struct{}),)*
                _ => panic!("invalid opcode {}", o)
            };
            return (op, mode);
        }
    }
}

optab!{
    99 => OpTerm,
    1 => OpAdd [mem] (pc) (a, b, c) {
        mem[c as usize] = mem[a as usize] + mem[b as usize];
    },
    2 => OpMul [mem] (pc) (a, b, c) {
        mem[c as usize] = mem[a as usize] * mem[b as usize];
    },
    3 => OpIn [mem] (pc) (a) {
        let mut line = String::new();
        io::stdin().lock().read_line(&mut line).unwrap();
        mem[a as usize] = line.trim().parse::<i32>().unwrap();
    },
    4 => OpOut [mem] (pc) (a) {
        println!("{}", mem[a as usize]);
    },
    5 => OpJpT [mem] (pc) (cond, dest) {
        if mem[cond as usize] != 0 {
            *pc = (mem[dest as usize] - 3) as usize
        }
    },
    6 => OpJpF [mem] (pc) (cond, dest) {
        if mem[cond as usize] == 0 {
            *pc = (mem[dest as usize] - 3) as usize
        }
    },
    7 => OpLT [mem] (pc) (a, b, c) {
        mem[c as usize] = if mem[a as usize] < mem[b as usize] {1} else {0};
    },
    8 => OpEQ [mem] (pc) (a, b, c) {
        mem[c as usize] = if mem[a as usize] == mem[b as usize] {1} else {0};
    }
}

impl OpCode for OpTerm {
    fn exec(&self, _mem: &mut Vec<i32>, _base: &mut usize,
            _mode: Vec<ParamMode>){}
    fn do_halt(&self) -> bool {true}
}

fn prog_exec(prog: &Vec<i32>) -> () {
    let mut prinst = prog.clone();
    let mut pc = 0;
    loop {
        let (op, mode) = opcode_from_i32(prinst[pc]);
        op.exec(&mut prinst, &mut pc, mode);
        if op.do_halt() {break;}
    }
}

fn main() {
    let f = io::BufReader::new(fs::File::open("5in").unwrap());
    let prog = f.split(',' as u8)
        .filter_map(
            |o| str::from_utf8(&o.ok()?).ok()?.trim().parse::<i32>().ok()
        ).collect::<Vec<i32>>();
    prog_exec(&prog);
}
