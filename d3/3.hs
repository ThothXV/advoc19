import qualified Data.Text as T
import qualified Data.Text.IO as I
import qualified Data.List as L
import Data.Text.Read
import Data.Maybe
import Control.Monad.Writer

type Point = (Int,Int)
data Line = HLine Point Int
          | VLine Point Int deriving (Eq, Show)
type PathLink = Writer (Sum Int) Line
type Intersect = Writer (Sum Int) Point

instance (Num a, Num b) => Num (a,b) where
  (a, b) + (c, d) = ((a+c), (b+d))
  (a, b) * (c, d) = ((a*c), (b*d))
  (a, b) - (c, d) = ((a-c), (b-d))
  abs (a, b) = ((abs a), (abs b))
  signum (a, b) = (signum a, signum b)
  fromInteger i = (fromInteger i, fromInteger i)

line :: Point -> Point -> PathLink 
line p1 (0, v) = writer ((VLine p1 v), Sum (abs v))
line p1 (h, 0) = writer ((HLine p1 h), Sum (abs h))
line p1 p2 = error "nonvert/horizontal line"

mdist :: Point -> Int
mdist (a, b) = (abs a) + (abs b)

textPoint :: T.Text -> Point
textPoint t =
  let (d, tn) = fromMaybe  (error "no text in point") $ T.uncons t
  in
    (case decimal tn of
       Right (n, _) -> fromIntegral n
       Left _ -> error "invalid point")
    * (case d of
         'R' -> (1, 0)
         'L' -> ((-1), 0)
         'U' -> (0, 1)
         'D' -> (0, (-1))
         _ -> error "invalid point")

strpath :: T.Text -> [PathLink]
strpath text =
  let texpath = T.splitOn (T.pack ",") text
      buildpath pln cpt (inc : ps) =
        let cln = pln >> line cpt inc
        in cln : buildpath cln (cpt+inc) ps
      buildpath pln cpt [] = []
  in buildpath (writer (HLine (0,0) 0, Sum 0)) (0, 0) (map textPoint texpath)

unline :: Line -> (Point, Int)
unline (HLine pt ln) = (pt,ln)
unline (VLine pt ln) = (pt,ln)

pointSum :: Point -> Int
pointSum (a, b) = a + b

calcintersect :: PathLink -> PathLink -> Point -> Intersect
calcintersect ln1 ln2 pt =
  let (pt1, len1) = unline $ fst $ runWriter ln1
      (pt2, len2) = unline $ fst $ runWriter ln2
      tlen = getSum $ (execWriter ln1) + (execWriter ln2)
  in writer (pt, Sum (tlen - ((abs len1 - (pointSum $ abs (pt - pt1)))
                              + (abs len2 - (pointSum $ abs (pt - pt2))))))

ptintersect :: Line -> Line -> [Point]
ptintersect (HLine (x1, y1) d1) (HLine (x2, y2) d2)
  | y1 == y2 =
    map (\x -> (x, y1))
    [max (min (x1+d1) x1) (min (x2+d2) x2)..
                 min (max (x1+d1) x1) (max (x2+d2) x2)]
  | otherwise = []
ptintersect (VLine (x1, y1) d1) (VLine (x2, y2) d2)
  | x1 == x2 = map (\y -> (x1, y2))
               [max (min (y1+d1) y1) (min (y2+d2) y2)..
                 min (max (y1+d1) y1) (max (y2+d2) y2)]
  | otherwise = []
ptintersect (VLine p1 d1) (HLine p2 d2) =
  ptintersect (HLine p2 d2) (VLine p1 d1)
ptintersect (HLine (x1, y1) d1) (VLine (x2, y2) d2)
  | (min x1 $ x1+d1) <= x2
    && x2 <= (max x1 $ x1+d1)
    && (min y2 $ y2+d2) <= y1
    && y1 <= (max y2 $ y2+d2) = [(x2, y1)]
  | otherwise = []

intersect :: PathLink -> PathLink -> [Intersect]
intersect pl1 pl2 =
  map (calcintersect pl1 pl2)
  $ ptintersect (fst $ runWriter pl1) (fst $ runWriter pl2)

pathIntersections :: [PathLink] -> [PathLink] -> [Intersect]
pathIntersections pw1 pw2 = concat $ concatMap (\x -> map (intersect x) pw2) pw1

main = do
  path1 <- I.getLine
  path2 <- I.getLine
  let intersections = pathIntersections (strpath path1) (strpath path2)
    in do
    putStrLn $ show $ minimum $ L.delete 0
      $ map mdist $ map (fst . runWriter) intersections
    putStrLn $ show $ minimum $ L.delete 0
      $ map (getSum . execWriter) intersections
