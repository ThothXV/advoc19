(require 'uiop)
(defun ldorbits (file)
  (let ((orbits (make-hash-table)))
    (with-open-file (f file)
      (loop for line = (read-line f nil nil)
         until (null line)
         do
           (let ((orbit-pair
                  (apply #'cons
                         (reverse
                          (mapcar #'intern
                                  (uiop:split-string line :separator ")"))))))
             (setf (gethash (car orbit-pair) orbits) orbit-pair))))
    (maphash (lambda (k v)
               (rplacd (gethash k orbits)
                       (gethash (cdr v) orbits)))
             orbits)
    orbits))

(defun part-a (orbits)
  (loop for v being the hash-values of orbits
     sum (1+ (length (cdr v))))) ;1+ because the root is gone

(defun drop-while2 (f a b)
  (if (funcall f (car a) (car b))
      (drop-while2 f (cdr a) (cdr b))
      (values a b)))

(defun part-b (orbits)
  (multiple-value-bind (you san)
      (drop-while2 #'eq (reverse (cdr (gethash 'YOU orbits)))
                   (reverse (cdr (gethash 'SAN orbits))))
    (+ (length you) (length san))))

(let ((orbits (ldorbits "6in")))
  (format t "~a~%~a~%" (part-a orbits) (part-b orbits)))
