use std::io::prelude::*;
use std::io;
use std::str;

trait OpCode {
    fn exec(&self, mem:&mut Vec<u32>, pc: &mut usize);
    fn do_halt(&self) -> bool {false}
}

macro_rules! optab {
    { $($num:literal => $struct:ident
        $( [$mem:ident] ($($oarg:ident),*) $act:expr)?),* }
    => {
        $(
            struct $struct;
            $(
                impl OpCode for $struct {
                    fn exec(&self, $mem:&mut Vec<u32>, pc: &mut usize){
                        let mut argc = 0;
                        $(argc += 1; let $oarg = $mem[*pc+argc];)*
                        $act;
                        *pc += argc + 1;    
                    }
                }
            )?
        )*        
        fn opcode_from_u32(o:u32) -> Box<dyn OpCode> {
            match o {
                $($num => Box::new($struct{}),)*
                _ => panic!("invalid opcode {}", o)
            }
        }
    }
}

optab!{
    99 => OpTerm,
    1 => OpAdd [mem] (a, b, c) {
        mem[c as usize] = mem[a as usize] + mem[b as usize];
    },
    2 => OpMul [mem] (a, b, c) {
        mem[c as usize] = mem[a as usize] * mem[b as usize];
    }
}

impl OpCode for OpTerm {
    fn exec(&self, _mem: &mut Vec<u32>, _base: &mut usize){}
    fn do_halt(&self) -> bool {true}
}

fn prog_exec(prog: &Vec<u32>, noun: u32, verb: u32) -> u32 {
    let mut prinst = prog.clone();
    prinst[1] = noun;
    prinst[2] = verb;
    let mut pc = 0;
    loop {
        let op = opcode_from_u32(prinst[pc]);
        op.exec(&mut prinst, &mut pc);
        if op.do_halt() {break;}
    }
    prinst[0]
}

fn main() {
    let prog = io::stdin().lock().split(',' as u8)
        .filter_map(
            |o| str::from_utf8(&o.ok()?).ok()?.trim().parse::<u32>().ok()
        ).collect::<Vec<u32>>();
    println!("part 1: {}", prog_exec(&prog, 1, 1));
    let tgt = 19690720;
    for noun in 0..100 {
        for verb in 0..100 {
            if prog_exec(&prog, noun, verb) == tgt {
                println!("part 2: {}", (100*noun)+verb);
                std::process::exit(0);
            }
        }
    }
}
